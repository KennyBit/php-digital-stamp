<?php
require_once('dbConnect.php');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require_once 'vendor/grafika-master/src/autoloader.php';
require_once 'TextToImage.php';


use Grafika\Grafika;

//set image file name
$digital_stamp = "";
function generateRandomString($length = 20) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$file_name = generateRandomString();

if (isset($_POST['btn-generate'])) {

    $date = $_POST['date'];
    $generation_purpose = $_POST['generation_purpose'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    $query = "SELECT * FROM users WHERE email='$email' AND password='$password'";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result);
    $num_row = mysqli_num_rows($result);

    if($num_row>0) {

        $random_serial = rand(100000000,999999999);

        $user_id = $row['id'];
        $signature_image = $row['signature_image'];

        $originalDate = $date;
        $file_date = date("d-M-Y", strtotime($originalDate));
        $date = date("d M Y", strtotime($originalDate));

        //create img object
        $img = new TextToImage;
        $text = $date;

        $font_size = 10;
        $image_width = 250;
        $image_height = 40;
        $img->createImage($text, $font_size, $image_width, $image_height);
        //$image = $img->showImage();

        $img->saveAsPng($file_date,'images/');

        // Blend date
        $input1 = 'images/tmtevents&creations.png';
        $input2 = 'images/'.$file_date.'.png';
        $output = 'images/'.$file_name.'.png';

        $editor = Grafika::createEditor();

        $image1 = Grafika::createImage( $input1 );
        $image2 = Grafika::createImage( $input2 );
        $editor->blend( $image1, $image2, 'normal', 0.9, 'center',0,-5); // overlay blend, opacity 50%, center position
        $editor->save( $image1, $output );

        // Blend signature
        $input1 = $output;
        $input2 = 'images/'.$signature_image.'';
        $output = 'images/'.$file_name.'.png';

        $editor = Grafika::createEditor();

        $image1 = Grafika::createImage( $input1 );
        $image2 = Grafika::createImage( $input2 );
        $editor->blend( $image1, $image2, 'normal', 0.9, 'center', 0, -40); // overlay blend, opacity 50%, center position
        $editor->save( $image1, $output );

        // // Blend serial no
        $img = new TextToImage;
        $font_size = 13;
        $image_width = 180;
        $image_height = 40;
        $img->createImage($random_serial, $font_size, $image_width, $image_height);
        //$image = $img->showImage();

        $img->saveAsPng($random_serial,'images/');

        $input1 = $output;
        $input2 = 'images/'.$random_serial.'.png';
        $output = 'images/'.$file_name.'.png';

        $editor = Grafika::createEditor();

        $image1 = Grafika::createImage( $input1 );
        $image2 = Grafika::createImage( $input2 );
        $editor->blend( $image1, $image2, 'normal', 0.9, 'bottom-center', 0, -25); // overlay blend, opacity 50%, center position
        $editor->save( $image1, $output );



        $digital_stamp = $output;

        $insert_history = "INSERT INTO `stamp_history`(`user_id`, `generation_purpose`, `date`) VALUES ('$user_id','$generation_purpose','$date')";
        mysqli_query($con, $insert_history);
        
    }
    
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://bootadmin.net/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/datatables.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fullcalendar.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/bootadmin.min.css">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">

    <title>The Masters Touch Events</title>
</head>
<body class="bg-light">

<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <h1 class="text-center mb-4">TMT Events</h1>
            <div class="card">
                <div class="card-body">
                    <form method="post" action="index.php">
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="generation_purpose">Generation purpose</label>
                                    <input type="text" class="form-control" id="generation_purpose" name="generation_purpose" aria-describedby="generation_purpose_help" placeholder="Enter purpose" required>
                                    <small id="generation_purpose_help" class="form-text text-muted">Enter generation purpose</small>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="text" class="form-control" id="date" name="date" aria-describedby="date_help" placeholder="Enter date" required>
                                    <small id="date_help" class="form-text text-muted">Enter target date</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" id="email" name="email" aria-describedby="email_help" placeholder="Enter email address" required>
                                    <small id="email_help" class="form-text text-muted">Enter email address</small>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="password">password</label>
                                    <input type="password" class="form-control" name="password" id="password" aria-describedby="password_help" required>
                                    <small id="password_help" class="form-text text-muted">Enter your password</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <button type="submit" name="btn-generate" class="btn btn-primary">Generate stamp</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <a href="login.php">Admin login</a>
                            </div>
                        </div>
                    </form>

                    <?php
                    if($digital_stamp != "") {?>
                        <img src="<?php echo $digital_stamp; ?>?=<?php echo filemtime($digital_stamp)?>"/>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://bootadmin.net/js/jquery.min.js"></script>
<script src="https://bootadmin.net/js/bootstrap.bundle.min.js"></script>
<script src="https://bootadmin.net/js/datatables.min.js"></script>
<script src="https://bootadmin.net/js/moment.min.js"></script>
<script src="https://bootadmin.net/js/fullcalendar.min.js"></script>
<script src="https://bootadmin.net/js/bootadmin.min.js"></script>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $(function() {
        $("#date").datepicker(
            {
                dateFormat: 'dd-mm-yy'
            }
        );
    });
</script>

</body>

</html>
