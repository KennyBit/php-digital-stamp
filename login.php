<?php
session_start();
require_once('dbConnect.php');

if(isset($_POST['login'])) {
    $password = $_POST['password'];
    $email = $_POST['email'];

    $login_query = "SELECT * FROM `users` WHERE email = '$email' AND password='$password' AND is_admin = '1'";
    if(mysqli_query($con, $login_query)){

        //Retrieve user details
        $result_query = mysqli_query($con, $login_query);
        $row = mysqli_fetch_array($result_query);

        $_SESSION["id"] = $row['id'];
        $_SESSION["name"] = $row['name'];
        $_SESSION["email"] = $row['email'];

        header('Location: stamp_history.php');
        die();

    }else{
        // Login not successful
        echo '<script language="javascript">';
        echo 'alert("Login not successful. Enter correct details")';
        echo '</script>';

    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://bootadmin.net/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/datatables.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fullcalendar.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/bootadmin.min.css">

    <title>Deejoh Online</title>
</head>
<body class="bg-light">

<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-4">
            <h1 class="text-center mb-4">Deejoh Stamp</h1>
            <div class="card">
                <div class="card-body">
                    <form method="post" action="login.php">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Email adress">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="Password">
                        </div>

                        <div class="row">
                            <div class="col pr-2">
                                <button type="submit" name="login" class="btn btn-block btn-primary">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://bootadmin.net/js/jquery.min.js"></script>
<script src="https://bootadmin.net/js/bootstrap.bundle.min.js"></script>
<script src="https://bootadmin.net/js/datatables.min.js"></script>
<script src="https://bootadmin.net/js/moment.min.js"></script>
<script src="https://bootadmin.net/js/fullcalendar.min.js"></script>
<script src="https://bootadmin.net/js/bootadmin.min.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118868344-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-118868344-1');
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-4097235499795154",
        enable_page_level_ads: true
    });
</script>

</body>

</html>
