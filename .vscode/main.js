const current = document.querySelector('#current');
const imgs = document.querySelectorAll('.imgs img');
const opacity = 0.7;

// set first image opacity
imgs[0].style.opacity = opacity;

imgs.forEach(img =>
  img.addEventListener('click', imgClick));

function imgClick(e) {
  // reset the opacity
  imgs.forEach(img => (img.style.opacity = 1));

  // change current image to source of clicked img
  current.src = e.target.src;

  // add fadeIn class
  current.classList.add('fade-in');

  // reset fade-in class after .5 secs
  setTimeout(() => current.classList.remove('fade-in'), 500);

  //change the opacity to opacity var
  e.target.style.opacity = opacity;
}
// Navbar
$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 650) {
	    $(".navbar1").css("background" , "#F46A6A");
	  }

	  else{
		  $(".navbar1").css("background" , "transparent");
	  }
  })
})

$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 650) {
	    $(".navbar1 a").css("color" , "#000");
	  }

	  else{
		  $(".navbar1 a").css("color" , "white");
	  }
  })
})

$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 650) {
	    $(".navbar1 a:hover").css("color" , "white");
	  }

	  else{
		  $(".navbar1 a:hover").css("color" , "#000");
	  }
  })
})

$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 650) {
	    $(".navbar1").css("height" , "100px");
	  }

	  else{
		  $(".navbar1").css("height" , "300px");
	  }
  })
})


// Text write itself
function autoType(elementClass, typingSpeed){
  var thhis = $(elementClass);
  thhis.css({
    "position": "relative",
    "display": "inline-block"
  });
  thhis.prepend('<div class="cursor" style="right: initial; left:0;"></div>');
  thhis = thhis.find(".text-js");
  var text = thhis.text().trim().split('');
  var amntOfChars = text.length;
  var newString = "";
  thhis.text("|");
  setTimeout(function(){
    thhis.css("opacity",1);
    thhis.prev().removeAttr("style");
    thhis.text("");
    for(var i = 0; i < amntOfChars; i++){
      (function(i,char){
        setTimeout(function() {
          newString += char;
          thhis.text(newString);
        },i*typingSpeed);
      })(i+1,text[i]);
    }
  },1500);
}

$(document).ready(function(){
  // Now to start autoTyping just call the autoType function with the
  // class of outer div
  // The second paramter is the speed between each letter is typed.
  autoType(".type-js",200);
});

$('.count').each(function () {
  $(this).prop('Counter',0).animate({
      Counter: $(this).text()
  }, {
      duration: 4000,
      easing: 'swing',
      step: function (now) {
          $(this).text(Math.ceil(now));
      }
  });
});