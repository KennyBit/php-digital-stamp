<?php
/**
 * TextToImage class
 * This class converts text to image
 * 
 * @author	CodexWorld Dev Team
 * @link	http://www.codexworld.com
 * @license	http://www.codexworld.com/license/
 */
class TextToImage {
    private $img;
    
    /**
     * Create image from text
     * @param string text to convert into image
     * @param int font size of text
     * @param int width of the image
     * @param int height of the image
     */
    function createImage($text, $fontSize = 22, $imgWidth = 250, $imgHeight = 7){

        // Set the content-typ

// Create the image
        $this->img = imagecreatetruecolor(140, 30);

// Create a few colors
        $white = imagecolorallocate($this->img, 255, 255, 255);
        $grey = imagecolorallocate($this->img, 128, 128, 128);
        $black = imagecolorallocate($this->img, 0, 0, 0);
        $red = imagecolorallocate($this->img, 242, 21, 21);
        imagefilledrectangle($this->img, 0, 0, 399, 49, $white);

        $font = 'fonts/the_unseen.ttf';

        imagettftext($this->img, 18, 0, 10, 20, $red, $font, $text);

// Using imagepng() results in clearer text compared with imagejpeg()

        
//        //create the image
//        $this->img = imagecreatetruecolor($imgWidth, $imgHeight);
//
//        //create some colors
//        $white = imagecolorallocate($this->img, 255, 255, 255);
//        $red = imagecolorallocate($this->img, 242, 21, 21);
//        $grey = imagecolorallocate($this->img, 128, 128, 128);
//        $black = imagecolorallocate($this->img, 0, 0, 0);
//        imagefilledrectangle($this->img, 0, -30, $imgWidth - 1, 80 - 1, $white);
//
//        //break lines
//        $splitText = explode ( "\\n" , $text );
//        $lines = count($splitText);
//
//        $angle = 0;
//        foreach($splitText as $txt){
//            $textBox = imagettfbbox($fontSize,$angle,$font,$txt);
//            $textWidth = abs(max($textBox[2], $textBox[4]));
//            $textHeight = abs(max($textBox[5], $textBox[7]));
//            $x = (imagesx($this->img) - $textWidth)/2;
//            $y = ((imagesy($this->img) + $textHeight)/2)-($lines-2)*$textHeight;
//            $lines = $lines-1;
//
//            //add some shadow to the text
//            imagettftext($this->img, $fontSize, $angle, $x, $y, $grey, $font, $txt);
//
//            //add the text
//            imagettftext($this->img, $fontSize, $angle, $x, $y, $red, $font, $txt);
//        }
		return true;
    }
    
    /**
     * Display image
     */
    function showImage(){
        header('Content-Type: image/png');
        return imagepng($this->img);
    }
    
    /**
     * Save image as png format
     * @param string file name to save
     * @param string location to save image file
     */
    function saveAsPng($fileName = 'text-image', $location = ''){
        $fileName = $fileName.".png";
        $fileName = !empty($location)?$location.$fileName:$fileName;
        return imagepng($this->img, $fileName);
    }
    
    /**
     * Save image as jpg format
     * @param string file name to save
     * @param string location to save image file
     */
    function saveAsJpg($fileName = 'text-image', $location = ''){
        $fileName = $fileName.".jpg";
        $fileName = !empty($location)?$location.$fileName:$fileName;
        return imagejpeg($this->img, $fileName);
    }
}