<?php
require_once('dbConnect.php');

if(isset($_POST['submit'])) {
    $name = $_POST['name'];
    $phone_number = $_POST['phone_number'];
    $email = $_POST['email'];

    $is_admin = '0';
    if(isset($_POST['is_admin'])){
        $is_admin = '1';
    }


    $file_name = $_FILES['attachment']['name'];
    $value = explode(".", $file_name);
    $file_size =$_FILES['attachment']['size'];
    $file_tmp =$_FILES['attachment']['tmp_name'];
    $file_type=$_FILES['attachment']['type'];
    $file_ext=strtolower(end($value));

    $expensions= array("jpeg","jpg","png");
    if(in_array($file_ext,$expensions)=== false){
        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }

    if($file_size > 7097152){
        $errors[]='File size must be excately 2 MB';
    }
    if(empty($errors)==true) {
        move_uploaded_file($file_tmp, "images/$file_name");
        move_uploaded_file($file_tmp, "images/$file_name");
    }

    $password=rand(1000,9999);
    $register = "INSERT INTO `users`(`name`, `phone_number`, `email`, `password`, `is_admin`, `stamp_image`) VALUES ('$name','$phone_number','$email','$password',$is_admin,'$file_name')";
    if(mysqli_query($con, $register)){
        // Redirect to main site
        echo '<script language="javascript">';
        echo 'alert("User created successfully")';
        echo '</script>';

    }else{
        // Registration not successful
        echo '<script language="javascript">';
        echo 'alert("User creation not successful.Try again")';
        echo '</script>';

    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://bootadmin.net/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/datatables.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/fullcalendar.min.css">
    <link rel="stylesheet" href="https://bootadmin.net/css/bootadmin.min.css">

    <title>Roadshow Masters Stamp</title>
</head>
<body class="bg-light">

<nav class="navbar navbar-expand navbar-dark bg-primary">
    <a class="sidebar-toggle mr-3" href="#"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand" href="#">Roadshow Masters</a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a href="#" id="dd_user" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Eugene</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd_user">
                    <a href="#" class="dropdown-item">Profile</a>
                    <a href="#" class="dropdown-item">Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div class="d-flex">
    <div class="sidebar sidebar-dark bg-dark">
        <ul class="list-unstyled">
            <li><a href="stamp_history.php"> Stamp history</a></li>
            <li><a href="users.php"> Users</a></li>
        </ul>
    </div>

    <div class="content p-4">
        <h2 class="mb-4">Users</h2>


        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="users.php" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" aria-describedby="name_help" placeholder="Enter name">
                                        <small id="name_help" class="form-text text-muted">Enter name</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="phone_number">Phone number</label>
                                        <input type="number" class="form-control" id="phone_number" name="phone_number" aria-describedby="phone_number_help" placeholder="Enter phone number">
                                        <small id="phone_number_help" class="form-text text-muted">Enter phone number (254....)</small>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" aria-describedby="email_help" placeholder="Enter email">
                                        <small id="email_help" class="form-text text-muted">Enter your email</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" id="image" name="attachment" aria-describedby="image_help" placeholder="Attach image">
                                        <small id="image_help" class="form-text text-muted">Attach image</small>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="is_admin">Is admin?</label>
                                        <input type="checkbox" class="form-control" id="is_admin" name="is_admin" aria-describedby="is_admin_help">
                                        <small id="is_admin_help" class="form-text text-muted">Is admin</small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm">
                                    <button type="submit" class="btn btn-primary" name="submit">Create user</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-7">
                <div class="card mb-4">
                    <div class="card-body">
                        <table id="example" class="table table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="width: 40%;">Name</th>
                                <th>Email</th>
                                <th>Phone number</th>
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $query = "SELECT * FROM users ORDER BY id DESC";
                            $result = mysqli_query($con, $query);
                            while($row = mysqli_fetch_array($result))
                            {
                                echo '<tr>';
                                echo '<td>'.$row['id'].'</td>';
                                echo '<td style="width: 40%;">'.$row['name'].'</td>';
                                echo '<td>'.$row['email'].'</td>';
                                echo '<td>'.$row['phone_number'].'</td>';
                                echo '<td>
                                            <a href="#" class="btn btn-icon btn-pill btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                            <a href="#" class="btn btn-icon btn-pill btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-fw fa-trash"></i></a>
                                      </td>';
                                echo '</tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="https://bootadmin.net/js/jquery.min.js"></script>
<script src="https://bootadmin.net/js/bootstrap.bundle.min.js"></script>
<script src="https://bootadmin.net/js/datatables.min.js"></script>
<script src="https://bootadmin.net/js/moment.min.js"></script>
<script src="https://bootadmin.net/js/fullcalendar.min.js"></script>
<script src="https://bootadmin.net/js/bootadmin.min.js"></script>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118868344-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-118868344-1');
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-4097235499795154",
        enable_page_level_ads: true
    });
</script>

</body>
</html>
